var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var rev = require('gulp-rev');
var clean = require('gulp-clean');
var minify = require('gulp-minify-css');
var runSequence = require('run-sequence');
const chalk = require('chalk');

/**
 * Deletes all css files in dist folder
 */
gulp.task('clean:dist-css', function () {
  console.log('[gulp] Removing old production css files');
  return gulp.src('dist/*.css', { read: false })
    .pipe(clean());
});

/**
 * Compiles sass
 */
gulp.task('compile:sass', function () {
  console.log('[gulp] Compiling sass files');
  return gulp.src('static/styles/main.scss')
    .pipe(sass({ compress: true }))
    .pipe(gulp.dest('dist/temp'));
});

/**
 * Compiles css
 */
gulp.task('compile:css', function () {
  console.log('[gulp] Compiling css files');
  return gulp.src('static/libs/*.css')
    .pipe(minify())
    .pipe(gulp.dest('dist/temp'));
});

/**
 * Deletes old production files and creates new versioned css files
 */
gulp.task('rev:css', ['clean:dist-css', 'compile:css', 'compile:sass'], function () {
  console.log('[gulp] Generating main stylesheets ');
  return gulp.src([
    'dist/temp/bootstrap-3.3.7.min.css',
    'dist/temp/bootstrap-theme-3.3.7.min.css',
    'dist/temp/main.css'
  ])
    .pipe(concat('main.css'))
    .pipe(rev())
    .pipe(gulp.dest('dist'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      base: 'dist',
      merge: true
    }))
    .pipe(gulp.dest('dist'));
});

/**
 * Deletes all script.js files in dist folder
 */
gulp.task('clean:dist-js', function () {
  console.log('[gulp] Removing old production js files');
  return gulp.src('dist/script*.js', { read: false })
    .pipe(clean());
});

/**
 * Compiles js
 */
gulp.task('compile:js', function () {
  console.log('[gulp] Compiling js files');
  return gulp.src('static/libs/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/temp'));
});

/**
 * Deletes old production files and creates new versioned js files
 */
gulp.task('rev:js', ['clean:dist-js', 'compile:js'], function () {
  console.log('[gulp] Generating scripts file');
  return gulp.src([
    'dist/temp/jquery-2.2.4.min.js',
    'dist/temp/bootstrap-3.3.7.min.js'
  ])
    .pipe(concat('scripts.js'))
    .pipe(rev())
    .pipe(gulp.dest('dist'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      base: 'dist',
      merge: true
    }))
    .pipe(gulp.dest('dist'));
});

var watchingForChanges = function() {
  console.log(chalk.blue('[gulp] Watching for changes...'));
};

/**
 * Watches over static files
 */
gulp.task('watch', function () {
  watchingForChanges();

  // Watches over styles
  gulp.watch([
    'static/libs/*.css',
    'static/styles/*.scss'], function () {
    return runSequence('rev:css', 'clean:temp', function() {watchingForChanges()});
  });
  // Watches over scripts
  gulp.watch('static/libs/*.js', function () {
    return runSequence('rev:js', 'clean:temp', function() {watchingForChanges()});
  });
});

/**
 * Serves refreshing gulp for webpack-dev-server
 */
gulp.task('serve', function () {
  console.log(chalk.blue('[gulp] Serving refreshing gulp...'));
  runSequence('rev:css', 'rev:js', 'clean:temp', 'watch');
});

gulp.task('build', function() {
  console.log(chalk.blue('[gulp] Building gulp files...'));
  runSequence('rev:css', 'rev:js', 'clean:temp');
});

/**
 * Deletes folder with temporary css / js files
 */
gulp.task('clean:temp', function () {
  console.log('[gulp] Removing dist/temp folder...');
  return gulp.src('dist/temp', { read: false })
    .pipe(clean());
});