const config = {
  API_BASE_URL: process.env.API_BASE_URL || 'https://bluesystem3.blueghost.cz/rtsm_api'
};

export default config;
