import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import App from '../containers/App';
import Home from '../containers/Home';
import PageNotFound from '../containers/PageNotFound';

export default function (history, store) {
  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="*" component={PageNotFound} />
      </Route>
    </Router>
  );
}
