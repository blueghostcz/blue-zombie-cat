import Express from 'express';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { useRouterHistory, RouterContext, match } from 'react-router';
import { createMemoryHistory, useQueries } from 'history';
import compression from 'compression';
import Promise from 'bluebird';
import { Provider } from 'react-redux';
const cookieParser = require('cookie-parser');
import Immutable from 'immutable';

import configureStore from '../configureStore';
import createRoutes from '../routes/index';


const server = new Express();
const port = process.env.PORT || 3000;

// Paths to javascript files
let scriptSrcs;
// Path to stylesheet
let styleSrc;

// Assets version control
const refManifest = require('../../dist/rev-manifest.json');


// We want to use static files for production
if (process.env.NODE_ENV === 'production') {
  const assets = require('../../dist/webpack-assets.json');
  scriptSrcs = [
    `/${assets.vendor.js}`,
    `/${assets.app.js}`,
    `/${refManifest['scripts.js']}`
  ];
  styleSrc = `/${refManifest['main.css']}`;
} // Files from webpack-dev-server
else {
  scriptSrcs = [
    'http://localhost:3001/static/vendor.js',
    'http://localhost:3001/static/dev.js',
    'http://localhost:3001/static/app.js',
    `/${refManifest['scripts.js']}`
  ];
  styleSrc = `/${refManifest['main.css']}`;
}

// More settings
server.use(compression());
server.use(cookieParser());
server.use(Express.static(path.join(__dirname, '../..', 'dist')));

// View engine settings
server.set('views', path.join(__dirname, './'));
server.set('view engine', 'ejs');

server.get('*', (req, res, next) => {
  // History
  const history = useRouterHistory(useQueries(createMemoryHistory))();

  // Store
  let store;
  if (req.cookies.user !== undefined) {
    const initialState = {
      auth: Immutable.fromJS({
        loggedIn: true,
        badCredentials: false,
        user: JSON.parse(req.cookies.user)
      })
    };
    store = configureStore(initialState);
  } else {
    store = configureStore();
  }

  const routes = createRoutes(history, store);
  const location = history.createLocation(req.url);

  // Match location to react-router
  match({ routes, location }, (error, redirectLocation, renderProps) => {
    // Redirect user
    if (redirectLocation) {
      res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    } // Server error
    else if (error) {
      res.status(500).send(error.message);
    } // Route not found
    else if (renderProps == null) {
      res.status(404).send('Not found');
    } // Run application
    else {
      let [getCurrentUrl, unsubscribe] = subscribeUrl();
      const reqUrl = location.pathname + location.search;

      // Wait for async data for server rendering
      getReduxPromise().then(() => {
        // Prepare provider
        const reduxState = escape(JSON.stringify(store.getState()));
        const html = ReactDOMServer.renderToString(
          <Provider store={store}>
            { <RouterContext {...renderProps} /> }
          </Provider>
        );
        // Render app
        if (getCurrentUrl() === reqUrl) {
          res.render('index', { html, scriptSrcs, reduxState, styleSrc });
        } // Redirect
        else {
          res.redirect(302, getCurrentUrl());
        }

        unsubscribe();
      }).catch((err) => {
        unsubscribe();
        next(err);
      });

      // Fetch data if component has static fetchData method
      function getReduxPromise() {
        let { query, params } = renderProps;
        const comp = renderProps.components[renderProps.components.length - 1].WrappedComponent;
        const promise = comp.fetchData ?
          comp.fetchData({ query, params, store, history }) :
          Promise.resolve();

        return promise;
      }
    }
  });
  function subscribeUrl() {
    let currentUrl = location.pathname + location.search;
    const unsubscribe = history.listen((newLoc) => {
      if (newLoc.action === 'PUSH') {
        currentUrl = newLoc.pathname + newLoc.search;
      }
    });
    return [
      () => currentUrl,
      unsubscribe
    ];
  }
});

server.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).send('something went wrong ...');
});

console.log(`Server is listening to port: ${port}`);
server.listen(port);
