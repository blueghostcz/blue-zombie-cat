import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import apiMiddleware from './appMiddlewareApi';
import rootReducer from './reducers/index';

// Use the one that suits the environment
const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  apiMiddleware
)(createStore);

// Export final configuration
export default function configureStore(initialState) {
  const store = createStoreWithMiddleware(rootReducer, initialState);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers/index', () => {
      const nextRootReducer = require('./reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
