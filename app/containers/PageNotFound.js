import React, { Component } from 'react';
import { connect } from 'react-redux';

const PageNotFound = () => (
  <div>
    <p>Page not found</p>
  </div>
);

export default connect()(PageNotFound);
